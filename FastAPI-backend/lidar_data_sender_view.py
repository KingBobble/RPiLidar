from pyrplidar_v2 import PyRPlidar
from time import sleep
import logging
import threading
import matplotlib.pyplot as plt
import queue
import numpy as np

MAX_SAMPLE_COUNT = 2000
SERIAL_PORT = "COM3"
# Create a video buffer queue
lidar_buf = queue.Queue()

def lidar_data_sender():
    lidar = PyRPlidar()
    lidar.connect(port=SERIAL_PORT, baudrate=115200, timeout=3)

    lidar.stop()

    health = lidar.get_health()
    if health.status == "GOOD":
        print("health :", health)
    else:
        health = lidar.get_health()
        print("health :", health)

    info = lidar.get_info()
    print("info :", info)

    samplerate = lidar.get_samplerate()
    print("samplerate :", samplerate)

    scan_modes = lidar.get_scan_modes()
    print("scan modes :")
    for scan_mode in scan_modes:
        print(scan_mode)

    # Turn on lidar scanning mode
    lidar.turn_motor_on(True)
    sleep(2)
    scan_generator = lidar.start_scan_express(1) # express mode

    data = {"data_list":[]}
    data_format = {}
    count = 0

    while True:
        try:
            for scan in scan_generator():
                if count == MAX_SAMPLE_COUNT:
                    lidar_buf.put(data)
                    data = {"data_list":[]}
                    count = 0
                    break
                
                if scan.quality > 0 and scan.distance > 0:
                    data_format = {"angle": scan.angle, "distance": scan.distance, "quality": scan.quality}
                    data["data_list"].append(data_format)
                    count += 1
            sleep(3)
        except:
            break

    # Turn off scanning and disconnet serial comm of the lidar
    lidar.stop()
    lidar.disconnect()


def view_plot_receiver():
    # Setup the polar graph
    fig = plt.figure()
    ax = plt.subplot(111, projection='polar')
    ax.set_theta_direction(-1)
    ax.set_rlabel_position(-22.5)
    line = ax.scatter([0, 0], [0, 0], s=1, c=['#17becf'])
    ax.set_rmax(5000) # Max distance in mm
    ax.grid(True)
    plt.ion()
    plt.show()
    
    offsets = []
    count = 0

    while True:
        try:
            if not lidar_buf.empty():
                buf = lidar_buf.get()
                for scan in buf["data_list"]:
                    if count == MAX_SAMPLE_COUNT:
                        break
                    offsets.append((np.radians(scan["angle"]), scan["distance"]))
                    count += 1
                    
                line.set_offsets(offsets)
                offsets = []
                count = 0
                plt.pause(0.001)
        except:
            pass


if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)


    lidar_thread = threading.Thread(target=lidar_data_sender, daemon=True)
    lidar_thread.start()

    view_plot_receiver()
