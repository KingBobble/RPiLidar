import uvicorn
import logging
from fastapi import FastAPI, Request, HTTPException
from fastapi.staticfiles import StaticFiles
from datapath import STATIC_PATH
import database
import models
import view_plot


app = FastAPI(
    docs_url=None,  # Disable docs API
    redoc_url=None  # Disable redoc API
)


@app.get("/api/GetAllDataInfo")
def get_all_data_info():
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    data_response = database.get_all_data_info()

    if data_response is None:
        returnmessage["status"] = "error"
        returnmessage["error"] = "DATA_ERROR"
        returnmessage["messages"].append("Data was not able to be retrieved from the database.")
        raise HTTPException(status_code=400, detail=returnmessage)
    
    json_response = {"results": []}

    for data in data_response:
        data_json = database.convert_simple_data_to_json(data)
        json_response["results"].append(data_json)

    returnmessage["data"] = json_response

    return returnmessage


@app.post("/api/AddDataPoints")
def add_data_points(data: models.DataList):
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    status = database.add_data_points(data.data_list)

    if not status:
        returnmessage["status"] = "error"
        returnmessage["error"] = "DATA_ERROR"
        returnmessage["messages"].append("Data not able to be added to the database.")
        raise HTTPException(status_code=400, detail=returnmessage)

    return returnmessage


"""@app.get("/api/GetGraphData")
def get_graph_data():
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    lidar_data_sender.lidar_data_send_over_http()
    view_plot.view_plot_over_http()

    return returnmessage"""


app.mount("/api", StaticFiles(directory=STATIC_PATH), name="static")


if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    
    database.create_tables()

    """Run main program on command line: 
    
    uvicorn main:app --host 127.0.0.1 --port 8000 --workers 4

    main: the file main.py (the Python "module").
    app: the object created inside of main.py with the line app = FastAPI().
    --reload: make the server restart after code changes. Only use for development.
    
    Interactive API documentation: http://127.0.0.1:8000/docs

    Gunicorn + Uvicorn combo on command line:

    gunicorn main:app --workers 4 --worker-class uvicorn.workers.UvicornWorker --bind 127.0.0.1:8000
    """
    
    # localhost
    uvicorn.run("main:app", host="127.0.0.1", port=8000)

    # private ip address
    # uvicorn.run("main:app", host="0.0.0.0", port=8000)
