from pydantic import BaseModel


class DataPoints(BaseModel):
    # data_id: int | None = None
    angle: float | None = 0.0
    distance: float | None = 0.0
    quality: int | None = 0

class DataList(BaseModel):
  data_list: list[DataPoints]
