import json
import sqlite3
import os
import logging
from time import sleep, time
from datetime import datetime
from datapath import DB_PATH


MAX_ROW_SIZE = 2000
directory = os.path.dirname(DB_PATH)


def connect_to_database(timeout=5):
    now = time()

    try:
        os.stat(directory)
    except Exception as error:
        logging.exception("Exception occurred")
        os.mkdir(directory)

    while (time() - now) < timeout:
        try:
            conn = sqlite3.connect(DB_PATH)
            return conn
        except Exception as error:
            logging.exception("Exception occurred")
            sleep(0.1)
            continue

    return None


def commit_conn(conn, timeout=5):
    if conn is not None:
        now = time()

        while (time() - now) < timeout:
            try:
                conn.commit()
            except Exception as error:
                # print(error)
                logging.exception("Exception occurred")
                sleep(0.1)
                continue
            else:
                break


def create_tables():
    """Create all needed tables, to be ran at startup"""
    create_data_table()


def create_data_table():
    """Create the data table if it does not exist, to be ran at start up"""
    conn = connect_to_database()
    MAX_ROW_SIZE_NUM = 0

    if conn is not None:
        curr = conn.cursor()
        curr.execute("CREATE TABLE IF NOT EXISTS 'plot_data' ( \
            `data_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \
            `angle` REAL DEFAULT 0.0 NOT NULL, `distance` REAL DEFAULT 0.0 NOT NULL, \
            `quality` INTEGER DEFAULT 0 NOT NULL);")
        
        curr.execute("SELECT COUNT(*) FROM plot_data;")
        MAX_ROW_SIZE_NUM = curr.fetchone()[0]

        if MAX_ROW_SIZE_NUM != MAX_ROW_SIZE:
            for _ in range(MAX_ROW_SIZE):
                curr.execute("INSERT INTO 'plot_data' DEFAULT VALUES;")
        
        commit_conn(conn)
        conn.close()


def add_data_points(data):
    conn = connect_to_database()
    status = False

    if not conn is None:
        curr = conn.cursor()

        try:
            if len(data) == MAX_ROW_SIZE:
                command = "UPDATE plot_data SET angle=?, distance=?, quality=? WHERE data_id=?;"
                for count, data_set in enumerate(data):
                    curr.execute(command, (data_set.angle, data_set.distance, data_set.quality, count+1,))
                status = True
        except Exception as error:
            logging.exception("Exception occurred")

        commit_conn(conn)
        conn.close()

    return status


def get_all_data_info():
    conn = connect_to_database()
    all_data_points = None

    if conn is not None:
        curr = conn.cursor()

        command = "SELECT data_id, angle, distance, quality FROM 'plot_data' ORDER BY data_id;"

        curr.execute(command)

        try:
            all_data_points = curr.fetchall()
        except Exception as error:
            logging.exception("Exception occurred")
            all_data_points = None

        conn.close()

    return all_data_points


def convert_simple_data_to_json(tups):
    jsonvar = {}
    jsonvar["data_id"] = tups[0]
    jsonvar["angle"] = tups[1]
    jsonvar["distance"] = tups[2]
    jsonvar["quality"] = tups[3]

    return jsonvar
