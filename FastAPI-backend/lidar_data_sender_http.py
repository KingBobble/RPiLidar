from pyrplidar_v2 import PyRPlidar
from time import sleep
import logging
import requests


API_ENDPOINT = "http://127.0.0.1:8000/api/AddDataPoints"
MAX_SAMPLE_COUNT = 2000
SERIAL_PORT = "COM3"


def lidar_data_send_over_http():
    lidar = PyRPlidar()
    lidar.connect(port=SERIAL_PORT, baudrate=115200, timeout=3)

    lidar.stop()

    health = lidar.get_health()
    if health.status == "GOOD":
        print("health :", health)
    else:
        health = lidar.get_health()
        print("health :", health)

    info = lidar.get_info()
    print("info :", info)

    samplerate = lidar.get_samplerate()
    print("samplerate :", samplerate)

    scan_modes = lidar.get_scan_modes()
    print("scan modes :")
    for scan_mode in scan_modes:
        print(scan_mode)

    # Turn on lidar scanning mode
    lidar.turn_motor_on(True)
    sleep(2)
    scan_generator = lidar.start_scan_express(1) # express mode

    data = {"data_list":[]}
    data_format = {}
    count = 0

    while True:
        try:
            for scan in scan_generator():
                if count == MAX_SAMPLE_COUNT:
                    r = requests.post(url=API_ENDPOINT, json=data)
                    data = {"data_list":[]}
                    count = 0
                    # http_response = r.text
                    # print("The http response is:%s" % http_response)
                    break
                
                if scan.quality > 0 and scan.distance > 0:
                    data_format = {"angle": scan.angle, "distance": scan.distance, "quality": scan.quality}
                    data["data_list"].append(data_format)
                    count += 1
        except:
            break

    # Turn off scanning and disconnet serial comm of the lidar
    lidar.stop()
    lidar.disconnect()

if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    lidar_data_send_over_http()
