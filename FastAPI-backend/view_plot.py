import matplotlib.pyplot as plt
import numpy as np
import requests
from time import sleep


PLOT_PATH = "plot.png"
API_ENDPOINT = "http://127.0.0.1:8000/api/GetAllDataInfo"
MAX_SAMPLE_COUNT = 2000


def view_plot_over_http():
    # Setup the polar graph
    fig = plt.figure()
    ax = plt.subplot(111, projection='polar')
    ax.set_theta_direction(-1)
    ax.set_rlabel_position(-22.5)
    line = ax.scatter([0, 0], [0, 0], s=1, c=['#17becf'])
    ax.set_rmax(5000) # Max distance in mm
    ax.grid(True)
    plt.ion()
    plt.show()
    
    offsets = []
    count = 0

    while True:
        try:
            response = requests.get(API_ENDPOINT)
            print(response)
            request_content = response.json()

            for scan in request_content["data"]["results"]:
                if count == MAX_SAMPLE_COUNT:
                    break
                offsets.append((np.radians(scan["angle"]), scan["distance"]))
                count += 1
                    
            line.set_offsets(offsets)
            offsets = []
            count = 0
            plt.pause(0.001)
        except:
            pass
    # plt.savefig(PLOT_PATH, bbox_inches='tight')
    # plt.show()

if __name__ == "__main__":
    view_plot_over_http()
