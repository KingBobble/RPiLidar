from pyrplidar_v2 import PyRPlidar
from time import sleep
import logging
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation


SERIAL_PORT = "COM3"


def lidar_setup():
    lidar = PyRPlidar()
    lidar.connect(port=SERIAL_PORT, baudrate=115200, timeout=3)

    lidar.stop()
    sleep(1)

    health = lidar.get_health()
    if health.status == "GOOD":
        print("health :", health)
    else:
        health = lidar.get_health()
        print("health :", health)

    info = lidar.get_info()
    print("info :", info)

    samplerate = lidar.get_samplerate()
    print("samplerate :", samplerate)

    scan_modes = lidar.get_scan_modes()
    print("scan modes :")
    for scan_mode in scan_modes:
        print(scan_mode)

    return lidar
    

def plot_setup(lidar):
    # Setup the polar graph
    fig = plt.figure()
    ax = plt.subplot(111, projection='polar')
    ax.set_theta_direction(-1)
    ax.set_rlabel_position(-22.5)
    line = ax.scatter([0, 0], [0, 0], s=1, c=['#17becf'])
    ax.set_rmax(5000) # Max distance in mm
    ax.grid(True)

    # Turn on lidar scanning mode
    lidar.turn_motor_on(True)
    sleep(2)
    scan_generator = lidar.start_scan_express(2) # boost mode

    # Plot the points on the graph
    ani = animation.FuncAnimation(fig, update_line,fargs=(scan_generator, line), interval=50)
    plt.show()

    # Turn off scanning and disconnet serial comm of the lidar
    lidar.stop()
    lidar.disconnect()


def update_line(num, scan_generator, line):
    offsets = []

    for count, scan in enumerate(scan_generator()):
        if count == 2000:
            break
        offsets.append((np.radians(scan.angle), scan.distance))
        
    line.set_offsets(offsets)
    
    return line


if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    lidar = lidar_setup()
    plot_setup(lidar)
