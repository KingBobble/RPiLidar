//

import SwiftUI

struct HomeView: View {
    var body: some View {
        NavigationView {
            NavigationLink {
                CompassView()
                    .navigationBarHidden(false)
            } label: {
                Image(systemName: "arrowtriangle.forward.circle.fill")
                    .font(.system(size: 25))
                    .padding()
            }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
