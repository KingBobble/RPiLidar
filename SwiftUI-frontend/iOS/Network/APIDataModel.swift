//

import Foundation

struct MultipleLidarDataModel: Codable {
    let data: ResultsClass
    let error: String
    let messages: [String]
    let status: String
}

struct ResultsClass: Codable {
    let results: [LidarDataModel]
}

struct LidarDataModel: Codable, Identifiable {
    var id = UUID()
    let dataID, quality: Int
    let angle, distance: Float

    enum CodingKeys: String, CodingKey {
        case dataID = "data_id"
        case angle, distance, quality
    }
}
