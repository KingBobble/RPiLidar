//

import Foundation

enum Endpoint {
    private static let baseURL = "http://127.0.0.1:8000/api/"
    
    static func createURLRequest(url: Route, method: HTTPMethod, JSONbody: [String: Any]? = nil) throws -> URLRequest {
        
        let urlString = Endpoint.baseURL + url.rawValue
        guard let url = URL(string: urlString)
        else { throw Errors.cantCreateURL }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if let body = JSONbody {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: body, options: .fragmentsAllowed)
            } catch {
                throw Errors.cantCreateURL
            }
        }
        return request
    }
}

enum Route: Equatable {
    case lidarDataInfo
    
    var rawValue: String {
        switch self {
        case .lidarDataInfo:
            return "GetAllDataInfo"
        }
    }
}

enum HTTPMethod: String {
    case GET, POST
}

enum Errors: Error, LocalizedError {
    case cantCreateURL
    case cantReadURL
    
    var errorDescription: String?{
        switch self {
        case .cantCreateURL:
            return NSLocalizedString("Unable to create url request.", comment: "")
        case .cantReadURL:
            return NSLocalizedString("Unable to read url JSON body request.", comment: "")
        }
    }
}
