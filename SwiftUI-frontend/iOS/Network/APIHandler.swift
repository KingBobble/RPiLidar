//

import Foundation

final class APIHandler {
    
    func getLidarDataInfo<T:Decodable>(completion: @escaping (T)->()) {
        do {
            let request = try Endpoint.createURLRequest(url: .lidarDataInfo, method: .GET)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                if let data = data {
                    do {
                        let lidar_data = try JSONDecoder().decode(T.self, from: data)
                        
                        //print(lidar_data)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            completion(lidar_data)
                        }
                    } catch {
                        print(Errors.cantReadURL)
                    }
                }
            }
            task.resume()
        } catch {
            print(error.localizedDescription)
        }
    }
}
