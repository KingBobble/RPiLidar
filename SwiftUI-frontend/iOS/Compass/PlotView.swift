//

import SwiftUI

struct PlotView: View {
    @State var lidar_data: MultipleLidarDataModel?
    
    var body: some View {
        if let lidar_data = lidar_data {
            Path { p in
                for data in lidar_data.data.results {
                    let start = CGPoint(x: 250, y: 169)
                    //let start = CGPoint(x: 250, y: 200)
                    let distance = Double(data.distance) / 25.4 // convert mm to inches
                    let degrees = Double(data.angle)
                    let pt = CGPoint(x: start.x - sin(degrees*Double.pi/180) * distance, y: start.y - cos(degrees*Double.pi/180) * distance)
                    self.drawCircle(in: &p, at: pt, radius: 1)
                }
            }
            .fill(.blue)
            //.clipped()
        }
    }
    
    private func drawCircle(in path: inout Path, at point: CGPoint, radius: CGFloat) {
        path.move(to: CGPoint(x: point.x + radius, y: point.y))
        path.addArc(center: point, radius: radius, startAngle: Angle(degrees: 0), endAngle: Angle(degrees: 360), clockwise: false)
    }
}

struct PlotView_Previews: PreviewProvider {
    static var previews: some View {
        CompassView()
    }
}
