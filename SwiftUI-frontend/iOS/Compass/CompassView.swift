//

import SwiftUI
import Foundation

struct CompassView: View {
    @State var lidar_data: MultipleLidarDataModel?
    @State private var updateData = false
    @ObservedObject var plotState = PlotDataState()
    @Environment(\.verticalSizeClass) var verticalSizeClass: UserInterfaceSizeClass?
    @Environment(\.horizontalSizeClass) var horizontalSizeClass: UserInterfaceSizeClass?
    
    var body: some View {
        /*GeometryReader { proxy in
         if proxy.size.width > proxy.size.height {
         }
         }*/
        if horizontalSizeClass == .compact && verticalSizeClass == .regular {
            Text("iPhone Portrait")
        } else if horizontalSizeClass == .compact && verticalSizeClass == .compact {
            HStack {
                ZStack {
                    Circle()
                        .stroke(.black, lineWidth: 1)
                        .frame(width: 500, height: 300)
                    Circle()
                        .stroke(.black, lineWidth: 1)
                        .frame(width: 400, height: 250)
                    Circle()
                        .stroke(.black, lineWidth: 1)
                        .frame(width: 300, height: 200)
                    Circle()
                        .stroke(.black, lineWidth: 1)
                        .frame(width: 200, height: 150)
                    Circle()
                        .stroke(.black, lineWidth: 1)
                        .frame(width: 100, height: 100)
                    
                    ForEach(Marker.markers(), id: \.self) { marker in
                        MarkerView(marker: marker, compassDegress: 90)
                    }
                    if let lidar_data = plotState.lidar_data {
                        PlotView(lidar_data: lidar_data)
                    }
                }
                .frame(maxWidth: 350,
                       maxHeight: 350)
                .rotationEffect(Angle(degrees: 90))
                .statusBar(hidden: true)
                //.aspectRatio(1, contentMode: .fit)
                
                Spacer()
                
                VStack {
                    Toggle("Auto-Update", isOn: $updateData)
                        .onReceive([self.updateData].publisher.first()) { toggle in
                            if toggle {
                                plotState.getLidarData()
                                
                            }
                        }
                    Spacer()
                }
            }
        }
    }
}

struct CompassView_Previews: PreviewProvider {
    static var previews: some View {
        CompassView()
    }
}
