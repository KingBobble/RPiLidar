//

import SwiftUI

struct MarkerView: View {
    let marker: Marker
    let compassDegress: Double

    var body: some View {
        VStack {
            Text(marker.label)
                .fontWeight(.bold)
                .rotationEffect(self.textAngle())
            
            Spacer()
            
            /*
            Text(marker.degreeText())
                .fontWeight(.light)
                .rotationEffect(self.textAngle())
            */

            Capsule()
                .frame(width: self.capsuleWidth(),
                       height: 150)
                .foregroundColor(self.capsuleColor())
                .padding(.bottom, 20)
                /*.frame(width: self.capsuleWidth(),
                       height: 295)
                .foregroundColor(self.capsuleColor())
                .padding(.bottom, 25)*/

        }
        .rotationEffect(Angle(degrees: marker.degrees))
    }
    
    private func capsuleWidth() -> CGFloat {
        return self.marker.degrees == 0 || self.marker.degrees == 90 || self.marker.degrees == 180 || self.marker.degrees == 270 ? 3 : 1
    }

    private func capsuleColor() -> Color {
        return self.marker.degrees == 0 || self.marker.degrees == 90 || self.marker.degrees == 180 || self.marker.degrees == 270 ? .green : .gray
    }

    private func textAngle() -> Angle {
        return Angle(degrees: -self.compassDegress - self.marker.degrees)
    }
}

struct MarkerView_Previews: PreviewProvider {
    static var previews: some View {
        CompassView()
    }
}
