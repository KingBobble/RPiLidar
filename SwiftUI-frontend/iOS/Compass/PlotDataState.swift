//

import Foundation

class PlotDataState: ObservableObject {
    @Published var lidar_data: MultipleLidarDataModel?
    
    public func getLidarData() {
        APIHandler().getLidarDataInfo() { (lidar_data) in
            self.lidar_data = lidar_data
        }
    }
}
