//

import Foundation

struct Marker: Hashable {
    let degrees: Double
    let label: String

    init(degrees: Double, label: String = "") {
        self.degrees = degrees
        self.label = label
    }

    func degreeText() -> String {
        return String(format: "%.0f", self.degrees)
    }

    static func markers() -> [Marker] {
        return [
            Marker(degrees: 0, label: "E"),
            Marker(degrees: 45),
            Marker(degrees: 90, label: "S"),
            Marker(degrees: 135),
            Marker(degrees: 180, label: "W"),
            Marker(degrees: 225),
            Marker(degrees: 270, label: "N"),
            Marker(degrees: 315),
        ]
    }
}
