//

import SwiftUI

@main
struct RPiLidarApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
